#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk


class window():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana.glade")

        self.window = self.builder.get_object("window")
        self.window.set_default_size(800, 600)
        self.window.connect("destroy", Gtk.main_quit)

        self.text1 = self.builder.get_object("text1")
        self.text2 = self.builder.get_object("text2")
        self.sum = self.builder.get_object("sum")
        self.button_accept = self.builder.get_object("accept")
        self.button_reset = self.builder.get_object("reset")

        for event in ["activate", "changed"]:
            self.text1.connect(event, self.update)
            self.text2.connect(event, self.update)

        self.button_accept.connect("clicked", self.info)
        self.button_reset.connect("clicked", self.confirm_reset)

        self.window.show_all()

    def update(self, blah=None):
        text1 = self.text1.get_text()
        text2 = self.text2.get_text()

        self.sum.set_value(len(text1) + len(text2))

    def info(self, gdjfkljgdf=None):
        text1 = self.text1.get_text()
        text2 = self.text2.get_text()
        length = len(text1) + len(text2)
        text = "Texto 1:\n" + text1
        text += "\n\nTexto 2:\n" + text2
        text += "\n\nLargo de ambas cadenas: " + str(length)
        text += "\n\n¿Desea reiniciar los cuadros de texto y el contador a cero?\n"

        dialog = Gtk.MessageDialog(type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.YES_NO)
        dialog.format_secondary_text(text)

        def response(dialogo, response):
            dialog.close()

            if response == Gtk.ResponseType.YES:
                self.reset()

        dialog.connect("response", response)
        dialog.run()

    def confirm_reset(self, fsdas=None):
        dialog = Gtk.MessageDialog(type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.YES_NO)
        dialog.format_secondary_text("¿Está seguro de que quiere reiniciar los cuadros de texto?")

        def response(dialogo, response):
            dialog.close()

            if response == Gtk.ResponseType.YES:
                self.reset()

        dialog.connect("response", response)
        dialog.run()

    def reset(self, asdf=None):
        self.text1.set_text("")
        self.text2.set_text("")
        self.update()


if __name__ == "__main__":
    w = window()
    Gtk.main()
