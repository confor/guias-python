import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from database import Database
from collections import namedtuple

Compound = namedtuple('Compound', 'name, type, formula')

# el profe pidió un formato super específico en la guia asi que
# aquí lo convierto a algo mucho más manejable
class Data:
	def __init__(self):
		self.data = {}
		self.load()

		return

	def load(self):
		raw = Database.load('db.json')
		data = {}

		if raw is None:
			raise Exception('database is broken')

		types = list(raw.keys())
		idx = 0

		for type in types:
			arr = raw[type]

			for item in arr:
				name = item[0]
				formula = item[1]

				data[idx] = Compound(name, type, formula)
				idx += 1

		self.data = data
		self.types = types
		self.nextIdx = idx + 1

		return

	def save(self):
		data = {}

		for idx, compound in self.data.items():
			if compound.type not in data:
				data[compound.type] = []

			obj = [compound.name, compound.formula]
			data[compound.type].append(obj)

		saved = Database.save('db.json', data)

		if saved is False:
			raise Exception('database did not save')

		return

	def update(self, idx, newdata):
		if idx not in self.data:
			return None

		if self.data[idx] == newdata:
			return None

		if newdata.type not in self.types:
			self.types.append(newdata.type)

		self.data[idx] = newdata
		self.save()

		return

	def delete(self, compound):
		idx = self.get_id(compound)
		del self.data[idx]
		self.save()

		return

	def insert(self, newdata):
		if newdata.type not in self.types:
			self.types.append(newdata.type)

		idx = self.nextIdx
		self.data[idx] = newdata
		self.nextIdx += 1
		self.save()

		return

	def get_id(self, search):
		for idx, compound in self.data.items():
			if compound.name == search.name and compound.type == search.type and compound.formula == search.formula:
				return idx

		return None


class window:
	def __init__(self, data):
		self.data = data

		builder = Gtk.Builder()
		builder.add_from_file('succion.glade')
		window = builder.get_object('window')

		window.connect('destroy', Gtk.main_quit)
		window.show_all()
		window.maximize()

		builder.get_object('button-add').connect('clicked', self.add_item)
		builder.get_object('button-edit').connect('clicked', self.edit_item)
		builder.get_object('button-remove').connect('clicked', self.remove_item)

		self.listmodel = Gtk.ListStore(str, str, str)
		self.table = builder.get_object('table')
		self.table.set_model(model=self.listmodel)

		cell = Gtk.CellRendererText()
		titles = ['Compuesto', 'Tipo', 'Formula']
		for i in range(len(titles)):
			col = Gtk.TreeViewColumn(titles[i], cell, text=i)
			self.table.append_column(col)

		self.draw()

	def add_item(self, asdsdg=None):
		dialog(self.data).window.connect('destroy', lambda x: self.draw())
		return

	def edit_item(self, fjskfj=None):
		compound = self.get_selected()

		if compound is None:
			return

		dialog(self.data, compound).window.connect('destroy', lambda x: self.draw())

		return

	def remove_item(self, asdhjkahsd=None):
		compound = self.get_selected()

		if compound is None:
			return

		text = '¿Está seguro de que desea eliminar el elemento seleccionado?'
		text += '\n' + compound.name + ' (' + compound.type + '): ' + compound.formula

		dialog = Gtk.MessageDialog(type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.YES_NO)
		dialog.format_secondary_text(text)

		def response(dialogo, response):
			dialog.close()

			if response == Gtk.ResponseType.YES:
				self.data.delete(compound)
				self.draw()

		dialog.connect("response", response)
		dialog.run()

	def get_selected(self, evt=None):
		model, it = self.table.get_selection().get_selected()

		if model is None or it is None:
			return None

		name = model.get_value(it, 0)
		type = model.get_value(it, 1)
		formula = model.get_value(it, 2)

		return Compound(name, type, formula)

	def draw(self):
		# borrar todo, si es que hay algo
		if len(self.listmodel) > 0:
			for i in range(len(self.listmodel)):
				iter = self.listmodel.get_iter(0)
				self.listmodel.remove(iter)

		# dibujar todo de nuevo
		for idx, compound in self.data.data.items():
			self.listmodel.append([compound.name, compound.type, compound.formula])


class dialog:
	def __init__(self, data, compound=None):
		self.data = data

		if compound:
			self.compound = compound

		builder = Gtk.Builder()
		builder.add_from_file('dialogo.glade')
		self.window = builder.get_object('dialog')

		self.window.show_all()
		self.types = self.data.types

		self.message = builder.get_object('message')
		self.entry_type = builder.get_object('entry-type')
		self.entry_name = builder.get_object('entry-name')
		self.entry_formula = builder.get_object('entry-formula')
		self.button_cancel = builder.get_object('button-cancel')
		self.button_ok = builder.get_object('button-ok')

		self.listmodel = Gtk.ListStore(str)
		for type in self.types:
			self.listmodel.append([type])

		# https://python-gtk-3-tutorial.readthedocs.io/en/latest/combobox.html#example
		self.entry_type.set_model(model=self.listmodel)
		cell = Gtk.CellRendererText()
		# https://lazka.github.io/pgi-docs/#Gtk-3.0/classes/Box.html#Gtk.Box.pack_start
		self.entry_type.pack_start(cell, True)
		self.entry_type.add_attribute(cell, 'text', 0)

		self.button_ok.connect('clicked', self.save)
		self.button_cancel.connect('clicked', lambda e: self.window.close())

		if compound:
			self.entry_name.set_text(compound.name)
			self.entry_formula.set_text(compound.formula)
			# TODO: poner el tipo en combobox

		#for w in [self.entry_name, self.entry_formula]:
			#w.connect('changed', lambda e: self.validate() if w.get_text() != '' else None)

	def save(self, event=None):
		if self.validate() is not True:
			return

		name = self.entry_name.get_text()
		type = self.types[self.entry_type.get_active()]
		formula = self.entry_formula.get_text()

		if self.compound:
			idx = self.data.get_id(self.compound)
			self.data.update(idx, Compound(name, type, formula))
		else:
			self.data.insert(Compound(name, type, formula))

		self.window.close()

		return

	def validate(self, event=None):
		# self.entry_type.get_model()[self.entry_type.get_active()][0]

		if self.entry_type.get_active() == -1:
			self.warn('Debe seleccionar un tipo')
			return False

		if self.entry_name.get_text() == '':
			self.warn('Debe ingresar un nombre')
			return False

		if self.entry_formula.get_text() == '':
			self.warn('Debe ingresar una composicion')
			return False

		self.warn('')
		return True

	def warn(self, text):
		self.message.set_text(text)


if __name__ == '__main__':
	memes = Data()
	suculentos = window(memes)
	Gtk.main()
