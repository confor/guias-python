#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from datetime import date

# de profe fabio, sacado de wikipedia
# https://es.wikipedia.org/wiki/Anexo:Implementaciones_para_algoritmo_de_rut#Python
def digito_verificador(rut):
    value = 11 - sum([ int(a)*int(b)  for a,b in zip(str(rut).zfill(8), '32765432')])%11
    return {10: 'K', 11: '0'}.get(value, str(value))


class form:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('form.glade')

        def get(id):
            return self.builder.get_object(id)

        self.window = get('window')
        self.window.set_default_size(500, 500)
        self.window.connect('destroy', Gtk.main_quit)

        self.input_name = get('input_name')
        self.input_addr = get('input_addr')
        self.input_date = get('input_date')
        self.output_diff = get('output_diff')
        self.input_rut = get('input_rut')
        self.input_mail = get('input_mail')

        self.email_regex = re.compile('^(.+)@(.+)$')
        self.rut_regex = re.compile('^([\\d\\.\\, ]+)-[\\dKk]$')

        self.input_name.connect('changed', self.validate)
        self.input_addr.connect('changed', self.validate)
        self.input_rut.connect('changed', self.validate_rut)
        self.input_mail.connect('changed', self.validate)

        self.input_date.connect('day-selected', self.update_date)
        self.input_date.connect('day-selected-double-click', self.update_date)

        # poner el calendario en el dia de hoy
        today = date.today()
        self.input_date.select_month(today.month - 1, today.year)
        self.input_date.select_day(today.day)

        self.window.show_all()
        self.validate()

    def validate(self, event=None):
        # nombre, direccion
        if self.input_name.get_text() == '':
            self.input_addr.set_visibility(False)
            self.input_addr.set_editable(False)
        else:
            self.input_addr.set_visibility(True)
            self.input_addr.set_editable(True)

        # esto puede ser lento
        email = self.input_mail.get_text()
        if len(email) >= 3:
            if self.email_regex.match(email) is None:
                # email malo
                self.input_mail.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'dialog-error')
            else:
                # email valido
                self.input_mail.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, None)
        else:
            # email vacio
            self.input_mail.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, None)

    def validate_rut(self, event=None):
        rut = self.input_rut.get_text()

        if len(rut) < 3:
            return

        if self.rut_regex.match(rut) is None:
            # rut no tiene formato válido
            self.input_rut.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'dialog-error')
        else:
            # rut tiene formato válido. debemos verificarlo
            rut = rut.replace(',', '')
            rut = rut.replace('.', '')
            rut = rut.replace(' ', '')
            cut = rut.split('-', 2)
            start = cut[0]
            end = cut[1]

            if digito_verificador(start) == end:
                # rut valido
                self.input_rut.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, None)
            else:
                # rut invalido
                self.input_rut.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'dialog-error')

    def update_date(self, event=None):
        year, month, day = self.input_date.get_date()
        givendate = date(year, month + 1, day)
        today = date.today()

        diff = today - givendate
        diff = abs(diff.total_seconds())

        if diff > 31556926:
            diff /= 2629743
            self.output_diff.set_text('{:d} meses'.format(int(diff)))
        elif diff >= 86400:
            diff /= 86400
            self.output_diff.set_text('{:d} dias'.format(int(diff)))
        else:
            self.output_diff.set_text('{:d} segundos'.format(int(diff)))


if __name__ == '__main__':
    w = form()
    Gtk.main()
